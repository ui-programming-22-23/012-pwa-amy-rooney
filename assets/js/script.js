const canvas = document.getElementById("the_canvas")
const context = canvas.getContext("2d");

let speed = 3;
const scale = 10;
const width = 16;
const height = 18;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
const walkLoop = [0, 1, 0, 2];
const frameLimit = 7;
const starWidth = 32;
const starHeight = 32;
const username = localStorage.getItem('username');
const score = localStorage.getItem('score');


let currentLoopIndex = 0;
let frameCount = 0;
let currentDirection = 0;
let randomX = Math.abs(Math.floor(Math.random() * 1099) - 50);
let randomY = Math.abs(Math.floor(Math.random() * 499) - 50);
let randomStarX = Math.abs(Math.floor(Math.random() * 7));
let randomStarXSelect = randomStarX * 64;
let randomStarY = Math.abs(Math.floor(Math.random() * 4));
let randomStarYSelect = randomStarY * 64;
let scoreCount = 0;
if (score){
    scoreCount = score;
}

function addName() {
    let header = document.getElementById("main-header");
    header.innerHTML = "Hello " + username;
}

addName();

function randoPos(rangeX, rangeY, delta){
    this.x = Math.abs(Math.floor(Math.random() * rangeX) - delta);
    this.y = Math.abs(Math.floor(Math.random() * rangeY) - delta);
}

function newStar(){
    randomStarX = Math.abs(Math.floor(Math.random() * 7));
    randomStarXSelect = randomStarX * 64;
    randomStarY = Math.abs(Math.floor(Math.random() * 4));
    randomStarYSelect = randomStarY * 64;
    starPosition = new randoPos(1099, 499, 50);
}


let image = new Image();
image.src = "assets/img/ghostsprite.png";
let imageS = new Image();
imageS.src = "assets/img/skeleton2.png";
let imageStar = new Image();
imageStar.src = "assets/img/star.png";


//health bar
let hbwidth = 200;
let hbheight = 20;
let hbmax = 100;
let hbval = 100; //amount of health


function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.mvmtDirection = "None";
}

let player = new GameObject(image, 50, 150, 200, 200);
let skeleton = new GameObject(imageS, 450, 200, 100, 100);
let star = new GameObject(imageStar, randomX, randomY, 150, 150);

function clickDpadYellow() {
    gamerInput = new GamerInput("Up");
    console.log(event);
}

function clickDpadBlue() {
    gamerInput = new GamerInput("Left");
    console.log(event);
}

function clickDpadRed() {
    gamerInput = new GamerInput("Right");
    console.log(event);
}

function clickDpadGreen() {
    gamerInput = new GamerInput("Down");
    console.log(event);
}

function clickableDpadReleased() {
    gamerInput = new GamerInput("None");
}

let yellowButton = document.getElementsByClassName("yellow")[0];
let blueButton = document.getElementsByClassName("blue")[0];
let redButton = document.getElementsByClassName("red")[0];
let greenButton = document.getElementsByClassName("green")[0];

function GamerInput(input) {
    this.action = input;
}

let gamerInput = new GamerInput("None");

function input(event) { //recognise user input
    if(event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left 
                blueButton.classList.add("pressed");
                gamerInput = new GamerInput("Left");
                break; 
            case 38: // Up 
                yellowButton.classList.add("pressed");
                gamerInput = new GamerInput("Up");
                break; 
            case 39: // Right 
                redButton.classList.add("pressed");
                gamerInput = new GamerInput("Right");
                break;
            case 40: // Down 
                greenButton.classList.add("pressed");
                gamerInput = new GamerInput("Down");
                break; 
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
        redButton.classList.remove("pressed");
        blueButton.classList.remove("pressed");
        yellowButton.classList.remove("pressed");
        greenButton.classList.remove("pressed");
    }
}


function collision() {
    if ( player.x + scaledWidth >= skeleton.x &&
          player.x <= skeleton.x + skeleton.width &&
          player.y + scaledHeight >= skeleton.y &&
          player.y <= skeleton.y + skeleton.height)
    {
      console.log("collision");
      hbval -=1;
      context.globalCompositeOperation='destination-over'
      context.fillStyle="red";
      context.fillRect(0,0, canvas.width, canvas.height);
    }
    return null;
  }


  function writeScore(){
    let scoreString = "score: " + scoreCount;
    context.font = '22px sans-serif';
    context.fillText(scoreString, 800, 20)
}

function update() {
    collision();
    if (gamerInput.action === "Up") {
        if (player.y < 0){
            console.log("player at top edge");
        }
        else{
            player.y -= speed; // Move Player Up
        }
        currentDirection = 1;
    } else if (gamerInput.action === "Down") {
        if (player.y + scaledHeight > canvas.height){
            console.log("player at bottom edge");
        }
        else{
            player.y += speed; // Move Player Down
        }
        currentDirection = 0;
    } else if (gamerInput.action === "Left") {
        if (player.x < 0){
            console.log("player at left edge");
        }
        else{
            player.x -= speed; // Move Player Left
        }
        currentDirection = 2;
    } else if (gamerInput.action === "Right") {
        if (player.x + scaledWidth > canvas.width){

        }
        else{
            player.x += speed; // Move Player Right
        }
        currentDirection = 3;
    } else if (gamerInput.action === "None") {
    }
}


function drawFrame(image, frameX, frameY, canvasX, canvasY) {
    context.drawImage(image,
                  frameX * width, frameY * height, width, height,
                  canvasX, canvasY, scaledWidth, scaledHeight);
}


function animate() {
    if (gamerInput.action != "None"){
        frameCount++;
        if (frameCount >= frameLimit) {
            frameCount = 0;
            currentLoopIndex++;
            if (currentLoopIndex >= walkLoop.length) {
                currentLoopIndex = 0;
            }
        }      
    }
    else{
        currentLoopIndex = 0;
    }
    drawFrame(player.spritesheet, walkLoop[currentLoopIndex], currentDirection, player.x, player.y);
}

starPosition = new randoPos(1099, 499, 50);

function manageStar(){
  
  context.drawImage(imageStar, randomStarX*64, randomStarY*64, 64, 64, starPosition.x, starPosition.y, starWidth, starHeight);

  if (starPosition.x < player.x + scaledWidth && //collision from left to right
      starPosition.x + starWidth > player.x && // collision from right to left
      starPosition.y < player.y + scaledHeight && // collision from top to bottom
      starPosition.y + starHeight > player.y // collision from bottom to top
      ){
      console.log("collision!");
      scoreCount ++;
      localStorage.setItem("score", scoreCount);
      newStar();
  }
  
}

function drawHealthBar() {
    //background
    context.fillStyle = "#000000";
    context.fillRect(0,0,hbwidth,hbheight);

    //fill
    context.fillStyle = "#00FF00";
    var fillVal = Math.min(Math.max(hbval / hbmax,0),1);
    context.fillRect(0 ,0, fillVal* hbwidth, hbheight);
}

function draw() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(skeleton.spritesheet, 
                      skeleton.x,
                      skeleton.y,
                      skeleton.width,
                      skeleton.height);
    drawHealthBar();
    manageStar();
    animate();
    writeScore();
     
}

function gameloop(){
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}


window.requestAnimationFrame(gameloop);

window.addEventListener('keydown', input);
window.addEventListener('keyup', input);